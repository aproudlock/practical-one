
// Exam Practical One
// Abbie Proudlock

#include <iostream>
#include <conio.h>

using namespace std;

double SquareNumber(double inputNum);
double CubeNumber(double inputNum);

int main()
{
		double inputNum;
		int exponent;

		cout << "Enter a number: \n";
		cin >> inputNum;
		cout << "Would you like to square(2) your number, or cube it(3)? \n";
		cin >> exponent;

		while (exponent == 2)
		{
			double answer = SquareNumber(inputNum);
			cout << "The answer is: " << answer;
			exponent = 4;
		}
		while (exponent == 3)
		{
			double answer = CubeNumber(inputNum);
			cout << "The answer is: " << answer;
			exponent++;
		}
		(void)_getch();
		return 0;
}

double SquareNumber(double inputNum) 
{
	double answer = inputNum * inputNum;

	return answer;
}

double CubeNumber(double inputNum) 
{
	double cubedNum = inputNum * inputNum * inputNum;
	return cubedNum;
}